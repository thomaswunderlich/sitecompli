<?php

/**
 * The isNegative, isPositive could be put into a wrapper function with the addition of a parameter, but I think that
 * would actually make it more difficult to follow even if it would be slightly DRYer.
 *
 * @param array $unsorted
 * @return array|null
 */
function getConsecutive(array $unsorted)
{
    $return = array();
    $count = count($unsorted);
    for($i = 0; $i < $count; $i++)
    {
        $j = isPositive($i, $unsorted);
        if($j != $i)
        {
            $return[] = $i;
            $i = $j - 1;
        }

        $j = isNegative($i, $unsorted);
        if($j != $i)
        {
            $return[] = $i;
            $i = $j - 1;
        }

    }
    if(empty($return)) { return null;}
    return $return;
}

//Don't bother wrapping isPositive and isNegative into one function. It makes it more difficult to understand without
// having a significant benefit in terms of cleaner and DRY code

/**
 * @param $j
 * @param $unsorted
 * @return mixed
 */
function isPositive($j, $unsorted)
{
    if(!isset($unsorted[($j + 1)])) { return $j; }

    if($unsorted[$j] - $unsorted[($j + 1)] == 1)
    {
        $j++;
        return isPositive($j, $unsorted);
    }
    return $j;
}

/**
 * @param $j
 * @param $unsorted
 * @return mixed
 */
function isNegative($j, $unsorted)
{
    if(!isset($unsorted[($j + 1)])) { return $j; }

    if($unsorted[$j] - $unsorted[($j + 1)] == -1)
    {
        $j++;
        return isNegative($j, $unsorted);
    }
    return $j;
}

