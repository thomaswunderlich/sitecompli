====Set up ====
Make sure python 2.7 and scrapy are installed. This should be crosscompatible on linux, bsd, mac osx, windows. Google for installation instructions and then pray. This is best done with cheap whiskey on hand because python's package management makes running ruby on windows seem like a good idea by comparison.


===Usage===
Run ```scrapy crawl cnn``` from sitecompli2 root folder. This will pull down a copy of the cnn site for later possible use and will then print out some scraped data in csv format between scrapy debug data.

To use the data, redirect output to a file or pipe and run it through some unix utilities like cut to find the data you actually care about ie  ``` command > file ``` or ```command | cut -flag -flag | awk --flag ```





