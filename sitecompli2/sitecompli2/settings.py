# -*- coding: utf-8 -*-

# Scrapy settings for sitecompli2 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'sitecompli2'

SPIDER_MODULES = ['sitecompli2.spiders']
NEWSPIDER_MODULE = 'sitecompli2.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'sitecompli2 (+http://www.yourdomain.com)'
