# -*- coding: utf-8 -*-
import scrapy, re


class CnnSpider(scrapy.Spider):
    name = "cnn"
    allowed_domains = ["cnn.com"]
    start_urls = (
        'http://www.cnn.com/',
    )

    """ Parse the html. Currently this downloads a copy of the html and then
    parses through it for useful data. If this was going to be operationalized
    it would be worth putting real effort in creating rules for edge cases and
    for scraping useful data. The data is currently printed out in csv format
    for broad spectrum use """
    def parse(self, response):
        #Download copy of the html page. Not strictly necessary, but useful especially for debugging.
        filename = response.url.split("/")[-2]
        with open(filename, 'wb') as f:
            f.write(response.body)

        #target the data you want via xpath. Eww I know but we're writing a spider of CNN.
        #To really do this well would require getting either xpath or scrap selectors working with the semantic metadata.
        topic_regex_pattern = re.compile(ur'\/\d{4}\/\d{2}\/\d{2}\/\w+')
        topic_regex_pattern2 = re.compile(ur'\/\d{4}\/\d{2}\/\d{2}\/')
        for sel in response.xpath('//article'):

            #title = sel.xpath('').extract()
            link = sel.xpath('div/div/h3/a/@href').extract()
            headline = sel.xpath('div/div/h3/a/span[1]/text()').extract()
            listmeta = sel.xpath('div/@data-analytics').extract() # this is what cnn uses in their analytics platform to see the list. This may be useful but probably not
            #parse the url to get the topic slug b/c CNN
            topic = ''.join(link)
            topic = topic_regex_pattern.match(topic)
            if(topic != None):
                topic = topic.group(0)
                topic = re.split(topic_regex_pattern2, topic)
                topic = topic[1]
            else: #This should cover rules that haven't been implemented yet like cnn.money, video links, carasol, etc
                topic = ''

            #print in csv format for easy cutting via unix tools
            print  topic + ',' + ''.join(listmeta) + ',' + ''.join(headline) + ',' + ''.join(link)
