<?php
/**
 * Created by PhpStorm.
 * User: twunderlich
 * Date: 1/15/15
 * Time: 8:33 AM
 */

//normally use composer + namespaces but for simple project I'll use the slower includes
include_once 'sitecompli.php';
include_once 'Question2.php';

class Test extends PHPUnit_Framework_TestCase {

    protected $Question1;

    public function setUp()
    {
        //This should be a mock or possibly a test db. I simply haven't
        // implemented it
        $this->Question1 = new Question1($Mockdb);
    }

    /**
     * Test for Question1
     */
    function testGetRealEstateCodeDescriptions()
    {
        $testInput = array(
            'M3',
            'R3-2',
            'PARKNYS',
            'M1-3/R9'
        );
        $testResult = array('codes' => array(
            array('code' => 'R3-2', 'description' => 'Residential Districts'),
            array('code' => 'PARKNYS', 'description' => 'New York State Parks'),
            array('code' => 'M1-3/R9', 'description' => 'Mixed Manufacturing & Residential Districts'),
        ));
        $result =  $this->Question1->getRealEstateCodeDescriptions($testInput);
        $this->assertTrue(is_string($result)); //test for json
        $result = json_decode($result);
        $this->assertTrue(isset($result['codes']));
        foreach($result as $value) {
            //test independently since json is not ordered
            $this->assertTrue(in_array($value, $testResult));
        }

    }

    //Unit test for Q2
    function testGetConsecutive()
    {
        $testInput = array(1, 1, 3, 5, 6, 8, 10, 11, 10, 9, 8, 9, 10, 11, 7);
        $this->assertEquals(getConsecutive($testInput), array(3,6,7,10));
    }



}
