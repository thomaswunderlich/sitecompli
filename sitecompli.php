<?php
/**
 * Created by PhpStorm.
 * User: twunderlich
 * Date: 1/12/15
 * Time: 9:31 AM
 */

/**
 * This approach used regexes but was abandoned because it's too error-prone.
 * Alternate approaches would be to use a script to generate a rollup table or generate code.
 * A rollup table would be more maintainable as more markets are added but does require the overhead of a db, db
 * script etc
 *
 * @deprecated
 * @param array $codes
 * @return  string
 */
function getRealEstateCodeDescriptionsv1(array $codes)
{
    $result = array('codes' => array());
    foreach($codes as $code) {
        $description = 'Unknown';

        echo $code;
        switch($code) {

            //This explicitly does not include R10
            case(preg_match('/^R[1-9]-[1-9]$/', $code) ? true : false): //Matches R1-1
            case(preg_match('/^R[1-9][A-Z]$/', $code) ? true : false): //Matches R1A
            case(preg_match('/^R10[A-H]$/', $code) ? true : false): //Matches R10H
                $description = 'Residential Districts';
                break;

//            case(preg_match('/.../', $code)  ? true : false):
//                $description = 'Commercial Districts';
//                break;
//
//            //This needs to come before Manufacturing districts
//            case(preg_match('/.../', $code)  ? true : false):
//                $description = 'Mixed Manufacturing & Residential Districts';
//                break;
//
//            case(preg_match('/.../', $code)  ? true : false):
//                $description = 'Manufacturing Districts';
//                break;


            case('BPC'):
                $description = 'Battery Park City';
                break;
            case('PARK'):
                $description = 'New York City Parks';
                break;
            case('PARKNYS'):
                $description = 'New York State Parks';
                break;
            case('PARKUS'):
                $description = 'United States Parks';
                break;
            case('ZNA'):
                $description = 'Zoning Not Applicable';
                break;
            case('ZR 11-151'):
                $description = 'Special Zoning District';
                break;
            default:
                //log an error message using standard mechanism.
                error_log('Code '.$code.' was not matched');
                break;
        }
        $result['codes'][] = array('code' => $code, 'description' => $description);
    }
    return json_encode($result);

}

interface DB {

    public function create(array $fields);
    public function findOneByField($field, $value);
}

class Question1
{
    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * This assumes that you have some sort of db object that adheres to the DB interface above. This
     * could be switched over to a cache interface easily as well or to a different interface
     *
     * sql table should contain at the minimum: code, description but really should contain state as well
     *
     * @param $db
     */
    function createRealEstateCodeDescriptions()
    {
        //This function creates the Real Estate descriptions for each code
        //This section assumes that codes increment from R1-9 to R2-1. R1A increments to R1Z to R2A
        $commercialRange = range(2, 7);
        $this->db->create(array('code' => 'BPC', 'description' => 'Battery Park City'));
        $this->db->create(array('code' => 'PARK', 'description' => 'New York City Parks'));
        $this->db->create(array('code' => 'PARKNYS', 'description' => 'New York State Parks'));
        $this->db->create(array('code' => 'PARKUS', 'description' => 'United States Parks'));
        $this->db->create(array('code' => 'ZNA', 'description' => 'Zoning Not Applicable'));
        $this->db->create(array('code' => 'ZR 11-151', 'description' => 'Special Zoning District'));
        for($i=1; $i <= 9; $i++) {
            for($j =1; $j <= 9; $j++) {
                //Residential
                $this->db->create(array('code' => 'R'.$i.'-'.$j, 'description' => 'Residential Districts'));
                //commercial
                if(($i == 1 && $j >= 6) || $i ==8 && $j <= 4 || in_array($i, $commercialRange) ) {
                    echo 'C'.$i.'-'.$j.PHP_EOL;
                    $this->db->create(array('code' => 'C'.$i.'-'.$j, 'description' => 'Commercial Districts'));
                }

                //Manufacturing Districts
                if(($i < 3) || ($i == 3 && $j <= 2)) {
                    echo 'M' . $i . '-' . $j . PHP_EOL;
                    $this->db->create(array('code' => 'M' . $i . '-' . $j , 'description' => 'Manufacturing Districts'));
                }

                //Mixed
                if($i <= 6 && (($j >= 4) && ($j <=9) ) ) {
                    $this->db->create(array('code' => 'M1-' . $i .'/R' . ($j + 1), 'description' => 'Mixed Manufacturing & Residential Districts'));
                }
            }
        }
    }

    function getRealEstateCodeDescriptions(array $inputs)
    {
        $return = array();
        foreach($inputs as $input) {
            $result = $this->db->findOneByField('Code', $input);
            if($result) {
                $return[] = array($input => $result);
            }
        }

        return json_encode(array('codes' => $return));
    }

}


/**
 * Used during testing only. Not updated
 */
function runGetRealEstateCodeDescriptions()
{
    $testInput = array(
        'BPC',
        'R7A',
        'R8A',
        'C4-4A',
        'M3-2',
        'R8B',
        'C1-6A',
        'R7B',
        'R8X',
        'C1-7A',
        'PARK',
        'C1-9A',
        'R6',
        'C1-7',
        'C2-6',
        'R10',
        'C4-5',
        'C6-3X',
        'C1-6',
        'C6-2M',
        'C6-4M',
        'M2-4',
        'M1-5/R7X'
    );
    echo getRealEstateCodeDescriptions($testInput);
}

function testGetRealEstateCodeDescriptions()
{
    $testInput = array(
        'M3',
        'R3-2',
        'PARKNYS',
        'M1-3/R9'
    );
    return  getRealEstateCodeDescriptions($testInput) == '{
	"codes":[
		{"code":"M3", "description":"Manufacturing Districts"},
		{"code":"R3-2", "description":"Residential Districts"},
		{"code":"PARKNYS", "description":"New York State Parks"},
		{"code":"M1-3/R9", "description":"Mixed Manufacturing & Residential Districts"}
	]
}';
}





createRealEstateCodeDescriptions();



